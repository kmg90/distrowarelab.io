# Debian

## Screenshot


## Description and history
Debian started in August 1993 by Ian Murdock.
>

> Developed by Debian Project

| username | password |  |
|----------|----------|--|
|  |  |  |


## License and type

> The Debian Free Software Guidelines (DFSG)


## Packaging, sources, repositories and building

>


## Table

|                       |  |
|-----------------------|--|
| Homepage              | https://debian.org  |
| Homepage backup       |  |
| Based on              |  |
| Status                |  Active|
| Architecture          | x86_64 |
| Category              |  |
| Desktop (default)     |  |
| Desktop (available)   |  |
| Source                |  |
| Download              |  |
| Release model         | Fixed |
| Packaging             | Deb |
| Package management    |  |
| Installer             |  |
| Init                  |  |
| Shell                 |  |
| C library             |  |
| Core utils            |  |
| Compiler              |  |
| Language              |  |
| Country               |  |
| IRC                   |  |
| Forum                 |  |
| Mailing list          |  |
| Docs                  |  |
| Bugtracker            |  |
| Translation           |  |
| Donations             |  |
| Commercial            |  |
| Price                 | Free |
| Social/Contact        |  |
| Social                |  |
| Social                |  |
| ArchiveOS             |  |
| Distrowatch           |  |
| Wikipedia             |  |
| [on LWN.net](https://lwn.net/Distributions/) |  |
| Repology              |  |
| In the timeline       |  |


## Releases

* 


## Media coverage

* 


## About this page

* This page source can be found at:
* <https://gitlab.com/Distroware/distroware.gitlab.io/-/tree/master/docs/os/Linux//>
* <https://github.com/FabioLolix/distroware.gitlab.io/tree/master/docs/os/Linux//>
